'use strict';

const BunnyHole = require('../index');

let bunnyHole = new BunnyHole({
  host: '127.0.0.1',
  port: 5672,
  username: 'guest',
  password: 'guest'
}, {debug: console.log, error: console.error});

bunnyHole.on('ready', () => {
  bunnyHole.call('sumObjectProperties', {a: 1, b: 2, c: 3}, (err, result) => {
    if (err) return console.error('Error in RPC call - ', err);
    console.log('Sum:', result);
  });
});

bunnyHole.on('error', (err) => {
  throw err;
});
