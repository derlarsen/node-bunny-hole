'use strict';

const BunnyHole = require('../index');

let bunnyHole = new BunnyHole({
  host: '127.0.0.1',
  port: 5672,
  username: 'guest',
  password: 'guest'
}, {debug: console.log, error: console.error});



bunnyHole.on('ready', () => {
  bunnyHole.expose('sumObjectProperties', (params, done) => {
    let sum = 0;
    for (let i in params) {
      if (typeof params[i] === 'number') {
        sum += params[i];
      } else {
        return done(new Error('Object contains non-numeric values'));
      }
    }

    done(null, sum);
  }, {
    "type": "object",
    "properties": {
      "a": {
        "type": "number",
        "minLength": 1
      },
      "b": {
        "type": "number",
        "minLength": 1
      },
      "c": {
        "type": "number",
        "minLength": 1
      }
    },
    "required": [
      "a", "b", "c"
    ]
  });
});
