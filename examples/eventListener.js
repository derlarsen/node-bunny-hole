'use strict';

const BunnyHole = require('../index');

let bunnyHole = new BunnyHole({
  host: '127.0.0.1',
  port: 5672,
  username: 'guest',
  password: 'guest',
  eventExchangeName: 'events'
}, {debug: console.log, error: console.error});

bunnyHole.on('ready', () => {
  bunnyHole.listenEvent('someEvent', (params) => {
    console.log('Something happened with params:', params);
  });

});

bunnyHole.on('error', (err) => {
  throw err;
});
