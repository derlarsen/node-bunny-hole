A library that allows easy communication between services via AMQP

# Installation

`npm install bunny-hole`

## Intro

This module provides functionality to do RPC via AMQP as well as event handling. RPC is implemented following the mechanisms described in the official [RabbitMQ tutorial](http://www.rabbitmq.com/tutorials/tutorial-six-javascript.html).

## Create an instance of BunnyHole:

`new BunnyHole(options [,logger])`

If `logger` is specified it must provide two methods: `debug` and `error`.

Example:
```javascript
const BunnyHole = require('bunny-hole');

let bunnyHole = new BunnyHole(
  {
    host: '127.0.0.1',            // optional, default is '127.0.0.1'
    port: 5672,                   // optional, default is 5672
    username: 'guest',            // optional, default is 'guest'
    password: 'guest',            // optional, default is 'guest',
    eventExchangeName: 'events'   // optional, default is 'events'
  },
  {
    debug: console.log,
    error: console.error
  }
);
```

### Events

The BunnyHole instance will emit a `ready` event once it is ready to go. If there is an error it will emit an `error` event.

## Register a service

`bunnyHole.expose(name, function(params, done) {...} [,parameterSchema])`


`parameterSchema`, if specified, must be a valid [JSON Schema](http://json-schema.org/). If the incoming parameters don´t match the schema, the request is answered automatically with an error and `callback` won´t be executed. Validation is done via `tv4`, so if you need more info on how to use it have a look at the [NPM module spec](https://www.npmjs.com/package/tv4).


Example:
```javascript
bunnyHole.on('ready', () => {
  bunnyHole.expose('sumObjectProperties', (params, done) => {
    let sum = 0;
    for (let i in params) {
      if (typeof params[i] === 'number') {
        sum += params[i];
      } else {
        return done(new Error('Object contains non-numeric values'));
      }
    }

    done(null, sum);
  });
});
```

## Make an RPC call

`bunnyHole.call(name, params [, function(err, result) {...}])`


If you don´t expect a result or just want to fire and forget you can omit the callback function parameter.


Example:
```javascript
bunnyHole.on('ready', () => {
  bunnyHole.call('sumObjectProperties', {a: 1, b: 2, c: 3}, (err, result) => {
    if (err) return console.error('Error in RPC call - ', err);
    console.log('Sum:', result);
  });
});
```


## Listen for events

`bunnyHole.listenEvent(name, function (params[, routingKey])) {...} [,groupName] [,queueOptions])`

If the `groupName` parameter is set, the listener will be assigned to a listener pool. Listeners in a pool will be called in a round robin pattern when the event is triggered.

The `queueOptions` parameter is directly passed through to the creation of the AMQP event queue. See [here](https://www.npmjs.com/package/amqp#queue) for further information.

Example:

```javascript
bunnyHole.on('ready', () => {
  bunnyHole.listenEvent('someEvent', (params) => {
    console.log('Something happened');
  });
});
```

## Emit an event

`bunnyHole.emitEvent(name, params)`


Example:

```javascript
bunnyHole.on('ready', () => {
  bunnyHole.emitEvent('someEvent', {foo: 'bar'});
});
```
