'use strict';

const
  EventEmitter = require('events'),
  amqp         = require('amqplib/callback_api'),
  assert       = require('assert'),
  async        = require('async'),
  tv4          = require('tv4'),
  util         = require('util'),
  uuid         = require('uuid');

class BunnyHole extends EventEmitter {

  /**
   * @param {Object} config
   * @param {string} config.host - AMQP host
   * @param {number} config.port - AMQP port
   * @param {string} [config.username] - AMQP username
   * @param {string} [config.password] - AMQP password
   * @param {string} [config.eventExchangeName] - AMQP event exchange name
   *
   * @param {Object} logger
   * @param {Function} logger.debug
   * @param {Function} logger.error
   */
  constructor(config, logger) {

    super();

    if (logger) assert(logger.debug, 'Logger must provide "debug" method');
    if (logger) assert(logger.error, 'Logger must provide "error" method');

    this._id = uuid.v4();
    this._config = Object.assign({
      username: 'guest',
      password: 'guest',
      host: '127.0.0.1',
      port: 5672,
      eventExchangeName: 'events'
    }, config);

    this._amqpConnection = null;
    this._amqpChannel = null;
    this._amqpReplyCallbacks = new Map();
    this._hasEventExchange = false;
    this._hasResponseQueue = false;

    this._logger = logger || {
        debug: () => {
        },
        error: () => {
        }
      };

    const
      connectString = util.format(
        "amqp://%s:%s@%s:%s",
        this._config.username,
        this._config.password,
        this._config.host,
        this._config.port
      );

    async.waterfall([
      (next) => {
        amqp.connect(connectString, next);
      },
      (connection, next) => {
        this._amqpConnection = connection;
        this._amqpConnection.createChannel(next);
      },
      (channel, next) => {
        this._amqpChannel = channel;
        next();
      },
      (next) => {
        this._amqpConnection.on('close', () => {
          this.emit('error', new Error('AMQP connection closed unexpectedly'));
        });
        this._amqpConnection.on('error', (err) => {
          this.emit('error', new Error('AMQP connection error - ' + err));
        });
        this._amqpChannel.on('close', () => {
          this.emit('error', new Error('AMQP channel closed unexpectedly'));
        });
        this._amqpChannel.on('error', (err) => {
          this.emit('error', new Error('AMQP channel error - ' + err));
        });
        next();
      }
    ], (err) => {
      if (err) return this.emit('error', new Error('Initialization error - ' + err));
      this.emit('ready');
    });
  }

  /**
   * @param {string} name - Response queue name
   * @private
   */
  _registerResponseQueue(name) {
    if (this._hasResponseQueue) return;

    this._amqpChannel.assertQueue(name, {durable: false, autoDelete: true}, (err) => {
      if (err) return this.emit('error', 'Could not set up response queue - ' + err.message);

      this._hasResponseQueue = true;

      this._amqpChannel.consume(name, (msg) => {

        if (!msg
          || !msg.properties.correlationId
          || !this._amqpReplyCallbacks.has(msg.properties.correlationId)
          || msg.properties.contentType !== 'application/json'
        ) return;

        try {
          const payload = JSON.parse(msg.content.toString());

          this._amqpChannel.ack(msg);
          this._amqpReplyCallbacks.get(msg.properties.correlationId)(
            payload.error ? new Error(payload.error) : null,
            payload.error ? null : payload.result
          );
          this._amqpReplyCallbacks.delete(msg.properties.correlationId);
        } catch (e) {
          this._logger.debug('Invalid payload');
        }
      });
    });
  }

  _registerEventExchange(callback) {
    if (this._hasEventExchange) return callback(null);

    this._amqpChannel.assertExchange(this._config.eventExchangeName, 'topic', {durable: false}, err => {
      this._hasEventExchange = !err;
      callback(err);
    });
  }

  /**
   * @callback onCall
   * @param {Object} params
   * @param {Function} done
   */

  /**
   * @param {string} name
   * @param {Object} params
   * @param {onCall} [callback]
   */
  call(name, params, callback) {
    this._amqpChannel.checkQueue(name, (err, ok) => {
      if (err || !ok) {
        return this.emit('error', new Error('Queue "' + name + '" does not exist'));
      }

      let msgProps = {contentType: 'application/json'};

      if (typeof callback === 'function') {
        msgProps.correlationId = uuid.v4();
        msgProps.replyTo = util.format('%s:res', this._id);

        this._amqpReplyCallbacks.set(msgProps.correlationId, callback);

        this._registerResponseQueue(msgProps.replyTo);
      }

      this._logger.debug('Calling RPC "%s"', name);
      this._amqpChannel.sendToQueue(name, new Buffer(JSON.stringify(params)), msgProps);
    });

  }

  /**
   * @param {string} name
   * @param {Function} onCall
   * @param {Object} [expectedParamsSchema]
   */
  expose(name, onCall, expectedParamsSchema) {
    this._amqpChannel.assertQueue(name, {durable: false, autoDelete: true}, (err) => {
      if (err) return this.emit('error', 'Error when asserting queue - ', err.message);

      this._amqpChannel.consume(name, (msg) => {

        if (!msg
          || msg.properties.contentType !== 'application/json'
        ) return;

        try {
          const
            doReply = (msg.properties.replyTo && msg.properties.correlationId),
            payload = JSON.parse(msg.content.toString());

          this._logger.debug('Received RPC call on %s', name);

          // validate params
          if (expectedParamsSchema) {
            const validationResult = tv4.validateResult(payload, expectedParamsSchema);
            if (!validationResult.valid) {
              if (doReply) {
                this._amqpChannel.sendToQueue(
                  msg.properties.replyTo,
                  new Buffer(JSON.stringify({
                      error: validationResult.error
                        ? util.format(
                        'Parameter validation failed: %s (Path %s)',
                        validationResult.error,
                        validationResult.error.dataPath
                      )
                        : 'Parameter validation failed'
                    })
                  ),
                  {correlationId: msg.properties.correlationId, contentType: 'application/json'}
                );
              }
              return;
            }
          }

          onCall(payload, (err, result) => {
            if (doReply) {
              this._logger.debug('Replying call on %s', name);
              this._amqpChannel.sendToQueue(
                msg.properties.replyTo,
                new Buffer(JSON.stringify((err) ? {error: err.message} : {result: result})),
                {correlationId: msg.properties.correlationId, contentType: 'application/json'}
              );
            }
            this._amqpChannel.ack(msg);
          });
        } catch (e) {
          this._logger.debug('Invalid payload');
        }
      });
    });
  }

  /**
   * Registers a listener for an event
   * @param {string} eventName
   * @param {Function} callback - Will be called when event was received
   * @param {string} [groupName] - Listener group name.
   *                               Will be used to create queue name so that multiple clients can connect to same queue.
   * @param {Object} [queueOptions] - Will be passed through to event queue. See https://www.npmjs.com/package/amqp#queue.
   * @returns {AmqpEventBus}
   */
  listenEvent(eventName, callback, groupName, queueOptions) {

    if (typeof groupName === 'object') {
      queueOptions = groupName;
      groupName = null;
    }

    if (typeof queueOptions !== 'object') {
      queueOptions = null;
    }

    const queueName = `events.${eventName}.${groupName || uuid.v4()}`;

    async.series([
      (next) => {
        this._registerEventExchange(next);
      },
      (next) => {
        this._amqpChannel.assertQueue(queueName, {autoDelete: true, durable: false}, next);
      },
      (next) => {
        this._amqpChannel.consume(queueName, msg => {
          try {
            this._logger.debug('Received event %s with routing key %s', eventName, msg.fields.routingKey);
            callback(
              JSON.parse(msg.content.toString()),
              msg.fields.routingKey
            );
            this._amqpChannel.ack(msg);
          } catch (e) {
            this._logger.debug('Invalid payload');
          }
        }, {}, next);
      },
      (next) => {
        this._amqpChannel.bindQueue(queueName, this._config.eventExchangeName, eventName, {}, next);
      }
    ], err => {
      if (err) this.emit('error', err);
    });

    return this;
  }

  /**
   * Emits an event into the message queue
   * @param {string} eventName
   * @param {Object} data
   * @returns {AmqpEventBus}
   */
  emitEvent(eventName, data) {
    this._registerEventExchange(err => {
      if (err) return this.emit('error', err);
      this._amqpChannel.publish(this._config.eventExchangeName, eventName, new Buffer(JSON.stringify(data)));
      this._logger.debug('Emitted event %s', eventName);
    });
  }
}

module.exports = BunnyHole;
